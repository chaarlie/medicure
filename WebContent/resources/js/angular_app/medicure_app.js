var medicureApp = angular.module("medicureApp", [ 'ngRoute', 'ngCookies' ]);
medicureApp.config([ '$routeProvider', function($routeProvider) {
	
	var base = '/medicure/resources/js/angular_app/partials/';
	
	$routeProvider.when('/', {
		templateUrl : base + 'home.html',
	}).when('/login', {
		templateUrl : base + 'login.html',
		controller : 'medicureMainController'
	}).when('/consults_form', {
		templateUrl : base + 'consults_form.html',
		controller : ''
	}).when('/medical_certificate', {
		templateUrl : base + 'medical_certificate.html',
		controller : ''
	}).when('/doctors_list', {
		templateUrl : base + 'doctors_list.html',
		controller : 'doctorsController'
	}).when('/consults_list', {
		templateUrl : base + 'consults_list.html',
		controller : 'medicureMainController'
	}).when('/register_doctor', {
		templateUrl : base + 'register_doctor.html',
		controller : 'doctorsController'
	}).when('/register_patient', {
		templateUrl : base + 'register_patient.html',
		controller : 'patientsController'
	});
} ]).factory(
		'handleLogin',
		function($http, Session) {
			return {
				doLogin : function(user) {
					return $http.post('login', user).then(
							function(res) {
								Session.create(res.data.name, res.data.lastName, res.data.role.name);
							},
							function(error){
								alert("error");
								console.log(error);
							}
					);
				}
			};
		}).service('Session', function($cookies) {
			this.create = function(name, lastName, role) {
				this.name = name;
				this.lastName = lastName;
				this.role = role;
				
				$cookies['name'] = name;
				$cookies['lastName'] = lastName;
				$cookies['role'] = role;	
				
			};
			this.destroy = function() {
				this.name = null;
				this.lastName = null;
				this.role = null;
			};
			return this;
		 }).directive('symptomSearchbox', function($http){
			 return{
				 templateUrl: '/medicure/resources/js/angular_app/directives_template/symptom_searchbox.html',
		 	 	 link:function(scope, element, attributes){
		 	 		 var searchPill = element.find('p');
		 	 		 searchPill.bind('keydown ', function(event){ 
		 	 			scope.symptomName = element.find('div').text().trim();
		 	 			console.log(scope.symptomName);
		 	 	 
		 	 			$http.get('find_symptom', {
		 	 				 params:{name: scope.symptomName}
		 	 			 })
		 	 			 	.success(function(data){
		 	 			 		scope.symptoms  = data;
		 	 			 		scope.showResults = true;
		 	 			 	})
		 	 			 	.error(function(data){
		 	 			 		console.log(data);
		 	 			 	});
		 	 		 });
		 	 		 
		 	 		 scope.$on('selected', function(event, id){
		 	 			 scope.$apply(function(){
		 	 				scope.showResults = false;
		 	 				searchPill.attr('contenteditable', 'false');
		 	 				alert("this is id: " + id);
		 	 			 });
		 	 		 });
		 	 	 }
			 }
		 }).directive('symptomElement', function(){
			 return{
				 scope:true,
				 template:'<li id="symptom.idSympton">{{symptom.name}}</li>',
				 link:function(scope, element, attribute){
					 element.click(function(){
						 alert('different 4');
						 $("#symptom-searchbox p").text(element.text());
						 $("#symptom-searchbox p").addClass('icon-cancel symptom-style');
						 scope.$emit('selected', attribute.id);
						 console.log(attribute );
					 });
				 }
			 }
		 });


medicureApp.controller('patientsController', function($scope, $routeParams,
		$http) {
});

medicureApp.controller('doctorsController', function($scope, $routeParams,
		$http) {
	
});

medicureApp.controller('medicureMainController', function($scope, $cookieStore, $routeParams, $cookies,
		$http, handleLogin, Session, $location ) {
	$scope.allSymptoms = [];
	Session.create($cookies['name'], $cookies['lastName'], $cookies['role']);
	$scope.session = Session;
	
	$scope.submitLogin = function() {
		if (!$scope.session) {
			handleLogin.doLogin($scope.user);
			$scope.session = Session;
			$location.path('/');
		}
		else{
			alert("should logout first");
		}
	}
});

medicureApp.controller('RouteController', function($scope, $routeParams) {

});
