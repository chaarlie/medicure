package org.medicure.dao;

import java.util.List;

import org.medicure.model.ConsultRelationship;
 
public interface ConsultRelationshipDao extends GenericDao<ConsultRelationship> {
	public ConsultRelationship findById(int idConsult);
	public List<ConsultRelationship> findAll();
	
}
