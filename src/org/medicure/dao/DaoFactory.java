package org.medicure.dao;

public class DaoFactory {

	private static ConsultRelationshipDao consultRelationshipDao;
	private static UserDao userDao  ;
	private static SymptomDao symptomDao  ;
	 
	public static ConsultRelationshipDao getConsultRelationshipDao() {
		return consultRelationshipDao;
	}
	public static void setConsultRelationshipDao(
			ConsultRelationshipDao consultRelationshipDao) {
		DaoFactory.consultRelationshipDao = consultRelationshipDao;
	}
	public static UserDao getUserDao() {
		return userDao;
	}
	public static void setUserDao(UserDao userDao) {
		DaoFactory.userDao = userDao;
	}
	public static SymptomDao getSymptomDao() {
		return symptomDao;
	}
	public static void setSymptomDao(SymptomDao symptomDao) {
		DaoFactory.symptomDao = symptomDao;
	}
}
