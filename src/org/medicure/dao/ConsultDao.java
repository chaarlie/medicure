package org.medicure.dao;

import java.util.List;

import org.medicure.model.Consult;
 

public interface ConsultDao extends GenericDao<Consult> {
	public List<Consult> findAllDoctors();
}