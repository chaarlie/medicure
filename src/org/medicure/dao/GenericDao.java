package org.medicure.dao;

public interface GenericDao<T> {

	public T create(T t);

	public void delete(Object id);

	public T find(Object id);

	public T update(T t);
}
