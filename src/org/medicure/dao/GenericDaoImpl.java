/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.medicure.dao;

import java.lang.reflect.Type;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

 
import org.springframework.transaction.annotation.Transactional;

import java.lang.reflect.ParameterizedType;
 
public class GenericDaoImpl<T> implements GenericDao<T> {
	 private Class<T> type;
	 
	 @SuppressWarnings("unchecked")
	 public GenericDaoImpl() {
		 Type t = getClass().getGenericSuperclass();
		 ParameterizedType pt = (ParameterizedType) t;
		 type = (Class<T>) pt.getActualTypeArguments()[0];
	 }
	
	@PersistenceContext
	protected EntityManager em;
	
	@Override
	@Transactional
	public T create(final T t) {
		em.persist(t);
		return em.contains(t)? t : null;
	}

	 @Override
	 public void delete(final Object id) {
	  em.remove(em.getReference(type, id));
	 }

	 @Override
	 public T find(final Object id) {
	  return em.find(type, id);
	 }

	 @Override
	 @Transactional
	 public T update(final T t) {
	  return em.merge(t);
	 }
}
