package org.medicure.dao;

import java.util.List;

import org.medicure.model.User;

public interface UserDao extends GenericDao<User>{
	public List<User> findAll();

	public User doAuthentication(User user);

	public List<User> findAllDoctors();

	public List<User>  findAllPatients(User doctor);

	 
}
