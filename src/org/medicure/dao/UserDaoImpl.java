package org.medicure.dao;

import java.util.List;

import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

import org.medicure.model.User;

public class UserDaoImpl extends GenericDaoImpl<User> implements UserDao {
	
	@Override
	public List<User> findAll() {
		TypedQuery<User> query = em.createQuery("SELECT u FROM User u", User.class);				
		return query.getResultList();
	}
	
	@Override
	public User doAuthentication(User u) {
		User foundUser = null;
		String dmlQuery = "SELECT NEW org.medicure.model.User("
						  +  " u.name, u.lastName,"
						  +  " u.cel, u.tel, u.username, u.email, u.role"
						  +  " )" +
						  " FROM User u WHERE" +
					      " u.username = :username AND u.password = :password";
		TypedQuery<User> query = em.createQuery(dmlQuery, User.class);
		query.setParameter("username", u.getUsername());
		query.setParameter("password", u.getPassword());
		
		try{
			foundUser = query.getSingleResult();
		}catch(NoResultException e){}
		
		return foundUser;
	}
	
	@Override
	public List<User> findAllDoctors(){
		List<User> doctors = null;
		String dmlQuery =  "SELECT NEW org.medicure.model.User("
						  +  " u.name, u.lastName,"
						  +  " u.cel, u.tel, u.username, u.email, u.role"
						  +  " ) " 
						  + " FROM User u WHERE u.role.idRole = :role ";
		TypedQuery<User> query = em.createQuery(dmlQuery, User.class);
		query.setParameter("role", 2);
		
		try{
			doctors = query.getResultList();
		}catch(NoResultException e){}
		return doctors;
	}

	@Override
	public List<User> findAllPatients(User doctor) {
		String dmlQuery = "";
		List<User> patients = null;
		
		if(doctor.getRole().getIdRole() == 2){
			dmlQuery =  "SELECT NEW org.medicure.model.User("
					+ " cr.attended.name,"
					+ " cr.attended.lastName,"
					+ " cr.attended.cel,"
					+ " cr.attended.tel,"
					+ " cr.attended.username,"
					+ " cr.attended.email,"
					+ " cr.attended.role)"
					+ " FROM  ConsultRelationship cr"
					+ " WHERE cr.attendant.idUser = :id ";			
		}
		TypedQuery<User> query = em.createQuery(dmlQuery, User.class);
		query.setParameter("id", doctor.getIdUser());
	 
		try{
			patients = query.getResultList();
		}catch(NoResultException e){}
		
		return patients;
	}
}
