package org.medicure.dao;

import java.util.List;

import org.medicure.model.Symptom;

public interface SymptomDao extends GenericDao<Symptom>  {
	List<Symptom> findByName(String name);
}
