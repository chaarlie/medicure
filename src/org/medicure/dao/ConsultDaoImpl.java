package org.medicure.dao;

import java.util.List;

import javax.persistence.TypedQuery;

import org.medicure.model.Consult;


public class ConsultDaoImpl extends GenericDaoImpl<Consult> implements ConsultDao{
	@Override
	public List<Consult> findAllDoctors(){
		TypedQuery<Consult> query = em.createQuery("SELECT c FROM Consult c", Consult.class);				
		return query.getResultList();
	}
}
