package org.medicure.dao;

 
import java.util.List;

import javax.persistence.TypedQuery;

import org.medicure.model.ConsultRelationship;
 
public class ConsultRelationshipDaoImpl extends GenericDaoImpl<ConsultRelationship> implements ConsultRelationshipDao{
	@Override
	public ConsultRelationship findById(int idConsult){
		return em.find(ConsultRelationship.class, idConsult);
	}
	@Override
	public List<ConsultRelationship> findAll(){
		TypedQuery<ConsultRelationship> query = em.createQuery("SELECT c FROM ConsultRelationship c", ConsultRelationship.class);
		List<ConsultRelationship> result = query.getResultList();
		return result;		 
	}
}
