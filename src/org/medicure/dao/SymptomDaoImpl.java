package org.medicure.dao;

import java.util.List;

import javax.persistence.TypedQuery;

import org.medicure.model.Symptom;

public class SymptomDaoImpl extends GenericDaoImpl<Symptom> implements SymptomDao {

	@Override
	public List<Symptom> findByName(String name) {
		if(name == "")
			return null; 
		TypedQuery<Symptom> query = em.createQuery("SELECT s FROM Symptom s WHERE s.name LIKE :name", Symptom.class);
		query.setParameter("name", name + "%");
		return query.getResultList();
	}
}
