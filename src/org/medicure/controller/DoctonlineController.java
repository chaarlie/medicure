package org.medicure.controller;

import java.util.*;

import javax.servlet.http.HttpSession;

import org.medicure.dao.DaoFactory;
import org.medicure.model.Consult;
import org.medicure.model.ConsultRelationship;
import org.medicure.model.Symptom;
import org.medicure.model.User;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

 

@Controller
public class DoctonlineController {	
	
	@RequestMapping("/")
	public String index(){
		return "index";
	}
	@RequestMapping("is_logged_in")
	public @ResponseBody User getLoggedIn(HttpSession session){
		return (User) session.getAttribute("user");
	}
	
	@RequestMapping("get_doctors")
	public @ResponseBody List<User> getDoctors(HttpSession session){
		User user = (User) session.getAttribute("user");
		return user != null? DaoFactory.getUserDao().findAllDoctors() : null;
	}
	
	@RequestMapping("get_patients")
	public @ResponseBody List<User> getPatients(HttpSession session){
		User doctor = (User) session.getAttribute("user");	
		return doctor != null? DaoFactory.getUserDao().findAllPatients(doctor) : null;
	}
	
	@RequestMapping(value = "login", method = RequestMethod.POST)
	public @ResponseBody User  doLogin(@RequestBody User user, HttpSession session){
		if(
		   !user.getUsername().isEmpty() && user.getUsername() != null && 
		   !user.getPassword().isEmpty() && user.getPassword() != null
		){
			user = DaoFactory.getUserDao().doAuthentication(user);
			if(user != null){
				session.setAttribute("user", user);
			}
		}
		return user;
	}
	
	@RequestMapping("get_consults")
	public @ResponseBody List<ConsultRelationship> findConsults(HttpSession session){
		User user = (User) session.getAttribute("user");
		return user != null? DaoFactory.getConsultRelationshipDao().findAll() : null;
	}
	
	@RequestMapping("create_user")
	public @ResponseBody User createUser(@RequestBody User user){
		return DaoFactory.getUserDao().create(user);
	}
	
	@RequestMapping("create_consult")
	public ConsultRelationship createConsult(
			@RequestBody User doctor, 
			@RequestBody User patient,
			@RequestBody Consult consult,
			@RequestBody User user,
			HttpSession session){
		ConsultRelationship cr = new ConsultRelationship();
		if(session.getAttribute("user") != null){
			cr.setAttendant(doctor);
			cr.setAttended(patient);
			cr.setConsult(consult);
			cr = DaoFactory.getConsultRelationshipDao().create(cr);
		}
		return cr;
	}
	
	@RequestMapping("find_symptom")
	public @ResponseBody List<Symptom>   searchSymptom (@RequestParam("name") String name){
		return DaoFactory.getSymptomDao().findByName(name);	
	}
	
}