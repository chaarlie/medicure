package org.medicure.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
 
@Entity
public class User {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id_user")
	private int idUser;
	private String name;
	@Column(name="last_name")
	private String lastName;
	private String cel;
	private String tel;
	private String email;
	private String password;
	private String username;
	  
	public User(){}
	
	public User(String name, String lastName, String cel, String tel,
			String username, String email, Role role) {
		super();
		this.name = name;
		this.lastName = lastName;
		this.cel = cel;
		this.tel = tel;
		this.email = email;
		this.username = username;
		this.role = role;
	}
	@ManyToOne(optional=false)
	@JoinColumn(name = "id_role", referencedColumnName = "id_role")
	private Role role;
	
	public int getIdUser(){
		return idUser;
	}
	public void setIdUser(int idUser){
		this.idUser = idUser;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getCel() {
		return cel;
	}
	public void setCel(String cel) {
		this.cel = cel;
	}
	public String getTel() {
		return tel;
	}
	public void setTel(String tel) {
		this.tel = tel;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public Role getRole() {
		return role;
	}
	public void setRole(Role role) {
		this.role = role;
	}
}
