package org.medicure.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Symptom {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id_symptom")
	private int idSympton;
	private String name;
	private String description;

	public int getIdSympton() {
		return idSympton;
	}
	public void setIdSympton(int idSympton) {
		this.idSympton = idSympton;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
 
}
