package org.medicure.model;

import javax.persistence.*;


public class Hospital {

	@Id
	@GeneratedValue
	@Column(name="id_hospital")
	private int idHospital;
	private String name;
	
	
	public int getIdHospital() {
		return idHospital;
	}
	public void setIdHospital(int idHospital) {
		this.idHospital = idHospital;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}	

}
