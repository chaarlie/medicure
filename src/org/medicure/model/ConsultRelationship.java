package org.medicure.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name="consult_relationship")
public class ConsultRelationship {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;

	@ManyToOne(optional=false)
	@JoinColumn(name="attendant")
	private User attendant;
	
	@ManyToOne(optional=false)
	@JoinColumn(name="attended")
	private User attended;
	
	@OneToOne
	@JsonManagedReference
	@JoinColumn(name="id_consult")
	private Consult consult;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public User getAttendant() {
		return attendant;
	}

	public void setAttendant(User attendant) {
		this.attendant = attendant;
	}

	public User getAttended() {
		return attended;
	}

	public void setAttended(User attended) {
		this.attended = attended;
	}

	public Consult getConsult() {
		return consult;
	}

	public void setConsult(Consult consult) {
		this.consult = consult;
	}

}
