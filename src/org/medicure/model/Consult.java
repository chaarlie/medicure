package org.medicure.model;


import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
public class Consult {

	@Id
	@GeneratedValue
	@Column(name="id_consult")
	private int idConsult;
	private String description;
	@Column(name="start_date")
	private Date startDate;
	@Column(name="end_date")
	private Date endDate;
	
	public Consult( String description, Date startDate,
			Date endDate) {
		super();
		this.description = description;
		this.startDate = startDate;
		this.endDate = endDate;
	 
	}
	public Consult(int id){
		idConsult = id;
	}
	public Consult(){}
	
	@OneToOne(mappedBy="consult")
	@JsonBackReference
	ConsultRelationship cr;	
	
	public int getIdConsult() {
		return idConsult;
	}
	public void setIdConsult(int idConsult) {
		this.idConsult = idConsult;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	/*public String getPhoto() {
		return photo;
	}
	public void setPhoto(String photo) {
		this.photo = photo;
	}*/
	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	public ConsultRelationship getCr() {
		return cr;
	}
	public void setCr(ConsultRelationship cr) {
		this.cr = cr;
	}
}
